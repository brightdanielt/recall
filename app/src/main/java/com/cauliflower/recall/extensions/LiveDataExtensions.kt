package com.cauliflower.recall.extensions

import androidx.lifecycle.MutableLiveData

/**
 * Make fields updating also notifies the observer.
 * */
fun <T> MutableLiveData<T>.mutation(action: (MutableLiveData<T>) -> Unit) {
    action(this)
    this.value = this.value
}

/**
 * @param repeatable means will the items repeat or not in the new value.
 * @param items the new items to be added into the existing list.
 * */
fun <T> MutableLiveData<List<T>>.plusAssign(
    repeatable: Boolean = true,
    items: List<T>
) {
    notifyObserver {
        this.value.plusAssign(repeatable, items)
    }
}

/**
 * @param item the item to be removed from the existing list.
 * */
fun <T> MutableLiveData<List<T>>.minusAssign(
    item: T
) {
    notifyObserver {
        this.value.minusAssign(item)
    }
}

/**
 * @param repeatable means will the items repeat or not in the new value.
 * @param items the new items to replace the existing list.
 * */
fun <T> MutableLiveData<List<T>>.replaceAssign(
    repeatable: Boolean = true,
    vararg items: T
) {
    val newList = items.toList()
    notifyObserver {
        if (repeatable) newList else newList.distinct()
    }
}

/**
 * Replace the existing value with a new value and notify the observer.
 * */
fun <T> MutableLiveData<T>.notifyObserver(newValue: ((MutableLiveData<T>) -> T)) {
    this.value = newValue.invoke(this)
}