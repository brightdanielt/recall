package com.cauliflower.recall.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.DisplayMetrics
import android.view.*
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * @param repeatable means will the items repeat or not in the new value.
 * @param items the new items to be added into the existing list.
 * */
fun <Item, R : List<Item>> R?.plusAssign(
    repeatable: Boolean = true,
    items: R
): R {
    val newList: MutableList<Item> = this?.toMutableList() ?: mutableListOf()
    newList += items
    return (if (repeatable) newList else newList.distinct()) as R
}

/**
 * @param item the item to be removed from the existing list.
 * */
fun <Item, R : List<Item>> R?.minusAssign(
    item: Item
): R {
    val newList: MutableList<Item> = this?.toMutableList() ?: mutableListOf()
    newList.remove(item)
    return newList as R
}

fun Date.toMyFormat(): String {
    return SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(this)
}

fun Activity.takeUriPermission(uri: Uri) {
    contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
}

fun Float.dpToPixel(context: Context): Float {
    return this * getDensity(context)
}

fun Float.pixelToDp(context: Context): Float {
    return this / getDensity(context)
}

fun Int.dpToPixel(context: Context): Int {
    return (this * getDensity(context)).toInt()
}

fun Int.pixelToDp(context: Context): Int {
    return (this / getDensity(context)).toInt()
}

fun Double.dpToPixel(context: Context): Double {
    return this * getDensity(context)
}

fun Double.pixelToDp(context: Context): Double {
    return this / getDensity(context)
}

fun getDensity(context: Context): Float {
    return context.resources.displayMetrics.density
}

fun AppCompatActivity.hideStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        window.decorView.windowInsetsController?.hide(WindowInsets.Type.statusBars())
    } else {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}

fun View.defaultDisplayMetrics(): DisplayMetrics {
    return DisplayMetrics().also {
        (context as Activity).windowManager.defaultDisplay.getMetrics(it)
    }
}

fun View.afterMeasured(doSomething: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            doSomething()
        }
    })
}

fun View.applySize(width: Int? = null, height: Int? = null) {
    val params = layoutParams
    width?.let { params.width = it }
    height?.let { params.height = it }
    layoutParams = params
}

/**
 * @param factor range is 0 <= factor <= 1
 * */
@ColorInt
fun @receiver:androidx.annotation.ColorInt Int.adjustAlpha(factor: Float): Int {
    return Color.argb(
        (Color.alpha(this) * factor).toInt(),
        Color.red(this),
        Color.green(this),
        Color.blue(this)
    )
}

/**
 * To prevent system bars overlap your app's view, we react to insets.
 * @see
 * https://developer.android.com/training/gestures/edge-to-edge
 * */
fun View.setOnApplyWindowInsetsListener() {
    ViewCompat.setOnApplyWindowInsetsListener(this) { view, windowInsets ->
        val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
        // Apply the insets as a margin to the view. Here the system is setting
        // only the bottom, left, and right dimensions, but apply whichever insets are
        // appropriate to your layout. You can also update the view padding
        // if that's more appropriate.
        view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
            leftMargin = if (insets.left == 0) marginLeft else insets.left
            bottomMargin = if (insets.bottom == 0) marginBottom else insets.bottom
            rightMargin = if (insets.right == 0) marginRight else insets.right
            topMargin = if (insets.top == 0) marginTop else insets.top
        }

        // Return CONSUMED if you don't want want the window insets to keep being
        // passed down to descendant views.
        WindowInsetsCompat.CONSUMED
    }
}