package com.cauliflower.recall.viewmodel

import androidx.lifecycle.*
import com.cauliflower.recall.db.Tag
import com.cauliflower.recall.db.TagDao
import com.cauliflower.recall.extensions.minusAssign
import com.cauliflower.recall.extensions.plusAssign
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class EditTagViewModel @AssistedInject constructor(
    @Assisted tagList: List<Tag>?,
    private val tagDao: TagDao
) : ViewModel() {

    private val _tags = MutableLiveData(tagList ?: listOf())
    val tags: LiveData<List<Tag>> = _tags

    val allTags = tagDao.getLiveDataTags().map { it.map { tag -> tag.name } }

    fun addTag(tag: Tag) = _tags.plusAssign(repeatable = false, listOf(tag))

    fun removeTag(position: Int) = _tags.minusAssign(_tags.value!![position])

    @AssistedFactory
    interface EditTagViewModelAssistedFactory {
        fun create(tagList: List<Tag>?): EditTagViewModel
    }

    class Factory(
        private val assistedFactory: EditTagViewModelAssistedFactory,
        private val tagList: List<Tag>?,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return assistedFactory.create(tagList) as T
        }
    }
}