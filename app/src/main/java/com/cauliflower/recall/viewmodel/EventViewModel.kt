package com.cauliflower.recall.viewmodel

import androidx.lifecycle.*
import com.cauliflower.recall.RecallRepository
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.db.NO_TAG
import com.cauliflower.recall.db.Tag
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EventViewModel @Inject constructor(
    private val repository: RecallRepository
) : ViewModel() {

    private val _allTags = repository.allLiveDataTags

    //Used in MaterialAlertDialogBuilder in menu.
    val allTags = _allTags.map { list -> list.map { it.name }.toTypedArray() }

    //Used in MaterialAlertDialogBuilder in menu.
    lateinit var filterBooleanArray: BooleanArray

    private val allEvents = repository.allEvent

    //default selection is NO_TAG.
    private val selectedTags = MutableStateFlow(listOf(NO_TAG))

    val filteredEvents = allEvents.combine(selectedTags) { events, tags ->
        events.filter { it.hasTag(tags) }
    }.asLiveData()

    fun addEvent(event: Event) = viewModelScope.launch {
        repository.addEvent(event)
    }

    fun deleteEvent(id: Int) = viewModelScope.launch {
        repository.deleteEvent(id)
    }

    private fun setFilter(tags: List<Tag>) {
        selectedTags.value = tags
    }

    fun submitTagFilter() {
        val newTagList = arrayListOf<Tag>()
        filterBooleanArray.forEachIndexed { index, checked ->
            if (checked) newTagList.add(_allTags.value!![index])
        }
        setFilter(newTagList)
    }

}