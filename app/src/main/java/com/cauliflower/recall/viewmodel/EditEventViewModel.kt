package com.cauliflower.recall.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cauliflower.recall.EditEvent
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.db.Tag
import com.cauliflower.recall.extensions.mutation
import com.cauliflower.recall.extensions.notifyObserver
import com.cauliflower.recall.extensions.plusAssign
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import java.util.*

class EditEventViewModel @AssistedInject constructor(
    @Assisted event: Event?
) : ViewModel(), EditEvent {

    private val _event = MutableLiveData(event ?: Event(Date(), "無標題"))
    val event: LiveData<Event> = _event

    override fun onAddTitle(text: String) {
        _event.value?.title = text
    }

    override fun onAddDescription(text: String?) {
        _event.value?.description = text
    }

    override fun onDateSet(year: Int, month: Int, dayOfMonth: Int) {
        _event.mutation { it.value?.time = Date(year - 1900, month, dayOfMonth) }
    }

    override fun onAddPictures(pathList: List<String>) {
        _event.value?.let {
            it.imagePathList = it.imagePathList.plusAssign(false, pathList)
        }
        _event.notifyObserver { it.value }
    }

    override fun onTagSelected(tags: List<Tag>) {
        _event.value?.tagList = tags
    }

    @AssistedFactory
    interface EditEventViewModelAssistedFactory {
        fun create(event: Event?): EditEventViewModel
    }

    class Factory(
        private val assistedFactory: EditEventViewModelAssistedFactory,
        private val event: Event?,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return assistedFactory.create(event) as T
        }
    }
}