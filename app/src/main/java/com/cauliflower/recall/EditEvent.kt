package com.cauliflower.recall

import com.cauliflower.recall.db.Tag

interface EditEvent {
    fun onAddTitle(text: String = "無標題")
    fun onAddDescription(text: String?)
    fun onDateSet(year: Int, month: Int, dayOfMonth: Int)
    fun onAddPictures(pathList: List<String>)
    fun onTagSelected(tags: List<Tag>)
}