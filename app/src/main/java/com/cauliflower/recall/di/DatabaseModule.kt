package com.cauliflower.recall.di

import android.content.Context
import androidx.room.Room
import com.cauliflower.recall.db.EventDao
import com.cauliflower.recall.db.RecallDatabase
import com.cauliflower.recall.db.TagDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    lateinit var database: RecallDatabase

    @Provides
    fun providesDatabase(
        @ApplicationContext context: Context,
        @RoomScope scope: CoroutineScope
    ): RecallDatabase {
        return Room.databaseBuilder(
            context,
            RecallDatabase::class.java,
            "Recall"
        )
            .addCallback(RecallDatabase.RecallDatabaseCallback(context, scope))
            .build().also { database = it }
    }

    @Provides
    fun providesTagDao(db: RecallDatabase): TagDao {
        return db.tagDao()
    }

    @Provides
    fun providesEventDao(db: RecallDatabase): EventDao {
        return db.eventDao()
    }
}