package com.cauliflower.recall.ui

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

/**
 * @param dragDirs
 * @param swipeDirs
 * Binary OR of direction flags in which the Views can be swiped. Must be composed of
 * [ItemTouchHelper.LEFT]
 * [ItemTouchHelper.RIGHT]
 * [ItemTouchHelper.START]
 * [ItemTouchHelper.END]
 * [ItemTouchHelper.UP]
 * [ItemTouchHelper.DOWN]
 */
class MyItemTouchCallback(
    dragDirs: Int = 0,
    swipeDirs: Int = 0,
    val actionMove: (position: Int) -> Boolean = { false },
    val actionSwipe: (position: Int) -> Unit = { }
) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return actionMove(viewHolder.adapterPosition)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        actionSwipe(viewHolder.adapterPosition)
    }

}