package com.cauliflower.recall.ui

import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import androidx.appcompat.widget.TooltipCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.recall.R
import com.cauliflower.recall.databinding.DialogFragmentEditTagBinding
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.db.Tag
import com.cauliflower.recall.ui.EditEventActivity.Companion.EXTRA_EVENT
import com.cauliflower.recall.viewmodel.EditTagViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * User can add new tags which means the [com.cauliflower.recall.db.Event.tagList]
 **/
@AndroidEntryPoint
class EditTagDialogFragment private constructor() : DialogFragment() {

    private var _binding: DialogFragmentEditTagBinding? = null
    private val binding get() = _binding!!
    private lateinit var tagsAdapter: TagsAdapter

    private lateinit var event: Event
    private lateinit var editTagListener: EditTagListener

    @Inject
    lateinit var assistedFactory: EditTagViewModel.EditTagViewModelAssistedFactory

    private val viewModel: EditTagViewModel by viewModels {
        EditTagViewModel.Factory(assistedFactory, event.tagList)
    }

    companion object {
        fun newInstance(event: Event, listener: EditTagListener): EditTagDialogFragment {
            return EditTagDialogFragment().also {
                val arg = Bundle()
                arg.putSerializable(EXTRA_EVENT, event)
                it.arguments = arg
                it.editTagListener = listener
                it.setStyle(STYLE_NORMAL, R.style.CustomDialog)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        event = arguments?.getSerializable(EXTRA_EVENT) as Event
        _binding = DialogFragmentEditTagBinding.inflate(inflater, container, false)

        binding.rvTags.let {
            tagsAdapter = TagsAdapter()
            it.adapter = tagsAdapter
            it.layoutManager = GridLayoutManager(
                requireContext(), 5, RecyclerView.VERTICAL, false
            )
            it.addItemDecoration(MarginItemDecoration(0, 5))
            //Swipe to remove Tag.
            val touchHelper = ItemTouchHelper(
                MyItemTouchCallback(
                    swipeDirs = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
                    actionSwipe = viewModel::removeTag
                )
            )
            touchHelper.attachToRecyclerView(it)
        }

        binding.btnAddTag.setOnClickListener {
            if (TextUtils.isEmpty(binding.autoCompleteTv.text)) return@setOnClickListener
            viewModel.addTag(Tag(binding.autoCompleteTv.text.toString()))
        }
        binding.btnSubmitTags.setOnClickListener { dismiss() }
        TooltipCompat.setTooltipText(binding.imgHint, getString(R.string.edit_tag_hint))
        dialog?.setTitle(R.string.edit_tag)
        dialog?.requestWindowFeature(Window.FEATURE_RIGHT_ICON)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.tags.observe(this) {
            tagsAdapter.swapData(it)
        }
        viewModel.allTags.observe(this) {
            binding.autoCompleteTv.setAdapter(
                ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_dropdown_item_1line,
                    it?.toMutableList() ?: arrayListOf()
                )
            )
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        editTagListener.onSubmit(tagsAdapter.list)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //prevent memory leak
        _binding = null
    }

    interface EditTagListener {
        fun onSubmit(list: List<Tag>)
    }
}