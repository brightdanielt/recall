package com.cauliflower.recall.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import androidx.activity.viewModels
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.WindowCompat
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.recall.R
import com.cauliflower.recall.databinding.ActivityMainBinding
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.extensions.hideStatusBar
import com.cauliflower.recall.extensions.setOnApplyWindowInsetsListener
import com.cauliflower.recall.ui.EditEventActivity.Companion.EXTRA_EVENT
import com.cauliflower.recall.ui.EditEventActivity.Companion.EXTRA_EVENT_ID
import com.cauliflower.recall.ui.EditEventActivity.Companion.EXTRA_SHARED_COLORS
import com.cauliflower.recall.ui.EditEventActivity.Companion.RS_DELETE_EVENT
import com.cauliflower.recall.viewmodel.EventViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val eventViewModel: EventViewModel by viewModels()
    private val eventAdapter by lazy { EventAdapter() }
    private lateinit var allTags: Array<String>

    companion object {
        private const val RQ_EDIT_EVENT = 500
        private const val RQ_ADD_EVENT = 501
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        WindowCompat.setDecorFitsSystemWindows(window, false)
        //Layout app behind system bars
        hideStatusBar()

        setContentView(binding.root)

        initView()
        subscribeViewModel()
    }

    /**
     * Get [Event] from result and add to RecallDatabase.
     * */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && (requestCode == RQ_EDIT_EVENT || requestCode == RQ_ADD_EVENT)) {
            (data?.getSerializableExtra(EXTRA_EVENT) as Event).let { eventViewModel.addEvent(it) }
        }
        if (resultCode == RS_DELETE_EVENT) {
            if (requestCode == RQ_ADD_EVENT) return
            if (requestCode == RQ_EDIT_EVENT) {
                data?.getIntExtra(EXTRA_EVENT_ID, -1)?.let { id ->
                    eventViewModel.deleteEvent(id)
                }
            }
        }
    }

    private fun initView() {
        binding.fabAddEvent.setOnApplyWindowInsetsListener()
        binding.fabAddEvent.setOnClickListener {
            val i = Intent(this@MainActivity, EditEventActivity::class.java)
            startActivityForResult(i, RQ_ADD_EVENT)
        }
        binding.btnMenu.setOnApplyWindowInsetsListener()
        binding.btnMenu.setOnClickListener {
            showMenu(it, R.menu.menu_main)
        }
        eventAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                binding.rvEvent.smoothScrollToPosition(positionStart)
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                binding.rvEvent.smoothScrollToPosition(toPosition)
            }
        })
        eventAdapter.onItemLongPressed = ::setPreviewImage
        eventAdapter.onItemClicked = ::startEditEventActivity
        binding.rvEvent.adapter = eventAdapter
    }

    private fun subscribeViewModel() {
        eventViewModel.filteredEvents.observe(this) { it?.let { eventAdapter.submitList(it) } }
        eventViewModel.allTags.observe(this) {
            allTags = it
            eventViewModel.filterBooleanArray = BooleanArray(allTags.size)
        }
    }

    private fun showMenu(view: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(this, view, menuRes)
        popup.menuInflater.inflate(menuRes, popup.menu)
        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            if (menuItem.itemId == R.id.action_set_filter) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(getString(R.string.choose_tag))
                    .setMultiChoiceItems(
                        allTags, eventViewModel.filterBooleanArray
                    ) { _, which, isChecked ->
                        eventViewModel.filterBooleanArray[which] = isChecked
                    }
                    .setPositiveButton(getString(R.string.ok)) { _, _ -> eventViewModel.submitTagFilter() }
                    .setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
                    .show()
            }
            return@setOnMenuItemClickListener true
        }
        popup.show()
    }

    private fun setPreviewImage(path: String?): Boolean {
//        Picasso.get().load(path).placeholder(R.drawable.ic_baseline_image_24).into(imgPreview)
        return true
    }

    /**
     * @param view the shared elements for transition.
     * @param sharedColors the shared colors to make transition more consistent.
     * */
    private fun startEditEventActivity(view: View, event: Event, sharedColors: IntArray) {
        Intent(this, EditEventActivity::class.java).let {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, view, getString(R.string.transition_name_preview_img)
            )
            it.putExtra(EXTRA_EVENT, event)
            it.putExtra(EXTRA_SHARED_COLORS, sharedColors)
            startActivityForResult(it, RQ_EDIT_EVENT, options.toBundle())
        }
    }

}