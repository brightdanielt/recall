package com.cauliflower.recall.ui

import android.content.Context
import android.content.res.Configuration
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.annotation.ColorRes
import com.cauliflower.recall.extensions.adjustAlpha
import kotlin.math.sqrt

/**
 * 自定義 ImageView，使用於 [item_event.xml]
 * */
class RingImageView(
    context: Context,
    attrs: AttributeSet? = null
) : androidx.appcompat.widget.AppCompatImageView(context, attrs) {

    private var direction = DIRECTION_TO_TOP

    private var gradientStart = Color.BLUE
    private var gradientEnd = Color.GREEN

    /**
     * It's elements are ColorInt.
     * */
    var sharedColors = intArrayOf(gradientStart, gradientEnd)
        private set

    private lateinit var extraBitmap: Bitmap
    private lateinit var extraCanvas: Canvas
    private val paintRing = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeWidth = STROKE_WIDTH
    }
    private val paintFilledCircle = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        val currentNightMode: Int =
            resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        color = when (currentNightMode) {
            Configuration.UI_MODE_NIGHT_YES -> Color.BLACK
            else -> Color.WHITE
        }
    }
    private val paintLine = Paint().apply {
        isAntiAlias = true
        strokeWidth = STROKE_WIDTH / 2
    }
    private val paintOval = Paint().apply {
        isAntiAlias = true
        strokeWidth = STROKE_WIDTH
        style = Paint.Style.FILL
    }
    private val paintArrow = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL_AND_STROKE
        color = Color.LTGRAY
    }
    private val paintRipple = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }

    private var circleCx = 0f
    private var circleCy = 0f
    private var circleRadius = 0f
    private var showRipple = false
    private var rippleRadius = 0f
    private var rippleRadiusMax = 0f

    companion object {
        const val STROKE_WIDTH = 10f
        const val DIRECTION_TO_TOP = 0
        const val DIRECTION_TO_BOTTOM = 1
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (::extraBitmap.isInitialized) extraBitmap.recycle()
        extraBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        extraCanvas = Canvas(extraBitmap)

        circleCx = width / 2f
        circleCy = height / 2f
        circleRadius = width / 4f

        rippleRadius = circleRadius
        rippleRadiusMax = circleRadius * 1.5f

        //Make target image always fits inside the ring.
        val paddingW = ((width - circleRadius * sqrt(2.0)) / 2).toInt()
        val paddingH = ((height - circleRadius * sqrt(2.0)) / 2).toInt()
        setPadding(paddingW, paddingH, paddingW, paddingH)
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas == null) {
            super.onDraw(canvas)
            return
        }
        //Clear canvas
        extraCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        drawMyContent()
        canvas.drawBitmap(extraBitmap, 0f, 0f, null)
        super.onDraw(canvas)
    }

    /**
     * 點擊特效
     * */
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) return super.onTouchEvent(event)
        //以圓的外切矩陣作為可觸發區域
        if (event.x > circleCx - circleRadius && event.x < circleCx + circleRadius &&
            event.y > circleCy - circleRadius && event.y < circleCy + circleRadius
        ) {
            if (event.action == MotionEvent.ACTION_DOWN) {
                showRipple = true
                invalidate()
            }
        }
        return false
    }

    /**
     * @return Side length of a square which fits in the ring.
     * */
    fun getHypotenuse(): Int {
        return (circleRadius * sqrt(2.0)).toInt()
    }

    /**
     * @param direction [DIRECTION_TO_TOP] or [DIRECTION_TO_BOTTOM]
     * */
    fun setDirection(direction: Int) {
        if (direction == this.direction) return
        this.direction = direction
        invalidate()
    }

    fun setGradientColor(@ColorRes start: Int, @ColorRes end: Int) {
        val newStart = resources.getColor(start, null)
        val newEnd = resources.getColor(end, null)
        if (gradientStart == newStart && gradientEnd == newEnd) return
        gradientStart = newStart
        gradientEnd = newEnd
        sharedColors = intArrayOf(gradientStart, gradientEnd)
        invalidate()
    }

    private fun getRingGradient(): LinearGradient {
        var y0 = 0f
        var y1 = 0f
        when (direction) {
            DIRECTION_TO_TOP -> {
                y0 = circleCy + circleRadius
                y1 = circleCy - circleRadius
            }
            DIRECTION_TO_BOTTOM -> {
                y0 = circleCy - circleRadius
                y1 = circleCy + circleRadius
            }
        }
        return LinearGradient(
            circleCx, y0,
            circleCx, y1,
            gradientStart, gradientEnd,
            Shader.TileMode.MIRROR
        )
    }

    private fun getLineGradient(): LinearGradient {
        var y0 = 0f
        var y1 = 0f
        when (direction) {
            DIRECTION_TO_TOP -> {
                y0 = circleCy - circleRadius
                y1 = 0 + STROKE_WIDTH * 2
            }
            DIRECTION_TO_BOTTOM -> {
                y0 = circleCy + circleRadius
                y1 = height - STROKE_WIDTH * 2
            }
        }
        return LinearGradient(
            circleCx, y0,
            circleCx, y1,
            gradientEnd, gradientEnd,
            Shader.TileMode.MIRROR
        )
    }

    private fun drawMyContent() {
        //Arrow
        extraCanvas.let {
            val side = circleRadius / 4
            val path = Path()
            path.moveTo(0f, circleCy)
            path.lineTo(side, circleCy - side)
            path.lineTo(width.toFloat(), circleCy - side)
            path.lineTo(width - side, circleCy)
            path.lineTo(width.toFloat(), circleCy + side)
            path.lineTo(side, circleCy + side)
            path.close()
            it.drawPath(path, paintArrow)
        }

        if (showRipple) drawRipple()

        //BigCircle
        extraCanvas.drawCircle(circleCx, circleCy, circleRadius, paintFilledCircle)

        //Ring
        extraCanvas.let {
            paintRing.shader = getRingGradient()
            it.drawCircle(circleCx, circleCy, circleRadius, paintRing)
        }

        //Line
        extraCanvas.let {
            var y0 = 0f
            var y1 = 0f
            when (direction) {
                DIRECTION_TO_TOP -> {
                    y0 = circleCy - circleRadius
                    y1 = 0f
                }
                DIRECTION_TO_BOTTOM -> {
                    y0 = circleCy + circleRadius
                    y1 = height.toFloat()
                }
            }
            paintLine.shader = getLineGradient()
            it.drawLine(circleCx, y0, circleCx, y1, paintLine)
        }

        //Circle in end
        extraCanvas.let {
            var cy = 0f
            when (direction) {
                DIRECTION_TO_TOP -> cy = 0f + STROKE_WIDTH
                DIRECTION_TO_BOTTOM -> cy = height - STROKE_WIDTH
            }
            paintOval.color = gradientEnd
            it.drawCircle(circleCx, cy, STROKE_WIDTH, paintOval)
        }
    }

    //Effect of Ripple
    private fun drawRipple() {
        //Increase the radius of ripple
        if (rippleRadius < rippleRadiusMax) {
            //Decrease the alpha, from (close to 2) to (close to 0)
            val alpha = (rippleRadiusMax - rippleRadius) / (circleRadius / 2)
            paintRipple.color = gradientStart.adjustAlpha(alpha)
            extraCanvas.drawCircle(circleCx, circleCy, rippleRadius, paintRipple)
            rippleRadius += 2
        } else {
            rippleRadius = circleRadius
            showRipple = false
        }
        invalidate()
    }

}