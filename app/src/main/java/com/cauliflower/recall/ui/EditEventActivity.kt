package com.cauliflower.recall.ui

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.WindowCompat
import com.cauliflower.recall.R
import com.cauliflower.recall.databinding.ActivityEditEventBinding
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.db.Tag
import com.cauliflower.recall.extensions.hideStatusBar
import com.cauliflower.recall.extensions.takeUriPermission
import com.cauliflower.recall.extensions.toMyFormat
import com.cauliflower.recall.viewmodel.EditEventViewModel
import com.google.android.material.appbar.AppBarLayout
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * User can create new event or edit event here.
 * There is a ViewFlipper which shows pictures on top of UI.
 *
 * Notice: In order to keep appbar top after scrolling up, you should set minHeight to motionLayout.
 * */
//https://developer.android.com/reference/android/widget/ViewFlipper.html
@AndroidEntryPoint
class EditEventActivity : AppCompatActivity(), EditTagDialogFragment.EditTagListener {

    private lateinit var binding: ActivityEditEventBinding

    @Inject
    lateinit var assistedFactory: EditEventViewModel.EditEventViewModelAssistedFactory

    private val viewModel: EditEventViewModel by viewModels {
        EditEventViewModel.Factory(
            assistedFactory,
            intent.getSerializableExtra(EXTRA_EVENT) as Event?
        )
    }

    companion object {
        const val RQ_SELECT_IMAGES = 600
        const val RS_DELETE_EVENT = 601
        const val EXTRA_EVENT = "event"
        const val EXTRA_SHARED_COLORS = "sharedColors"
        const val EXTRA_EVENT_ID = "event_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        hideStatusBar()
        binding = ActivityEditEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        subscribeViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(this).inflate(R.menu.menu_add_event, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                onBackPressed()
            }
            R.id.action_save -> {
                if (viewModel.event.value?.title?.isEmpty() == true) {
                    Toast.makeText(
                        this, getString(R.string.title_should_not_be_empty), Toast.LENGTH_SHORT
                    ).show()
                    return true
                }
                Intent().let {
                    it.putExtra(EXTRA_EVENT, viewModel.event.value)
                    setResult(RESULT_OK, it)
                    onBackPressed()
                }
            }
            R.id.action_set_date -> {
                MyDatePickerDialog(
                    this,
                    { _, year, month, dayOfMonth -> viewModel.onDateSet(year, month, dayOfMonth) },
                    viewModel.event.value!!.time
                ).show()
            }
            R.id.action_add_pic -> {
                Intent().let {
                    it.type = "image/*"
                    it.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                    it.action = Intent.ACTION_OPEN_DOCUMENT
                    startActivityForResult(Intent.createChooser(it, "選擇照片"), RQ_SELECT_IMAGES)
                }
            }
            R.id.action_add_tag -> {
                EditTagDialogFragment.newInstance(viewModel.event.value!!, this).show(
                    supportFragmentManager, EditTagDialogFragment::class.java.simpleName
                )
            }
            R.id.action_delete_event -> {
                Intent().let {
                    it.putExtra(EXTRA_EVENT_ID, viewModel.event.value?.id)
                    setResult(RS_DELETE_EVENT, it)
                    onBackPressed()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSubmit(list: List<Tag>) = viewModel.onTagSelected(list)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_SELECT_IMAGES && resultCode == RESULT_OK && data != null) {
            val pathList = arrayListOf<String>()
            //User select one item.
            data.data?.let { uri ->
                takeUriPermission(uri)
                pathList.add(uri.toString())
            }
            //User select more than one item.
            data.clipData?.let { it ->
                for (i in 0 until it.itemCount) {
                    it.getItemAt(i).uri.let { uri ->
                        takeUriPermission(uri)
                        pathList.add(uri.toString())
                    }
                }
            }
            viewModel.onAddPictures(pathList)
        }
    }

    private fun initView() {
        binding.viewFlipper.setOnClickListener { binding.viewFlipper.showNext() }

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        val appBarLayout = findViewById<AppBarLayout>(R.id.appbar_layout)
        val motionLayout = findViewById<MotionLayout>(R.id.motion_layout)

        appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
                val seekPosition = -verticalOffset / appBarLayout.totalScrollRange.toFloat()
                motionLayout.progress = seekPosition
            }
        )

        intent.getSerializableExtra(EXTRA_EVENT).let {
            binding.tvTitle.text = when (it) {
                null -> getString(R.string.add)
                else -> getString(R.string.edit)
            }
        }

        intent.getIntArrayExtra(EXTRA_SHARED_COLORS).let {
            motionLayout.background = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, it)
        }

        binding.edTxtTitle.addTextChangedListener(object : MyTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                s?.toString()?.let { viewModel.onAddTitle(it) }
            }
        })

        binding.edTxtContent.addTextChangedListener(object : MyTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                s?.toString()?.let { viewModel.onAddDescription(it) }
            }
        })
    }

    private fun subscribeViewModel() {
        viewModel.event.observe(this) { event ->
            binding.edTxtTitle.setText(event.title)
            binding.edTxtContent.setText(event.description)
            binding.tvDate.text = event.time.toMyFormat()
            event.imagePathList?.let {
                binding.viewFlipper.removeAllViews()
                for (path in it) addToViewFlipper(path)
            } ?: addToViewFlipper(null)
        }
    }

    private fun addToViewFlipper(path: String?) {
        ImageView(this).let {
            Picasso.get().load(path).placeholder(
                AppCompatResources.getDrawable(this, R.drawable.ic_baseline_image_24)!!
            ).into(it)
            binding.viewFlipper.addView(it)
        }
    }

}