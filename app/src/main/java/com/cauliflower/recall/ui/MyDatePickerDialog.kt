package com.cauliflower.recall.ui

import android.app.DatePickerDialog
import android.content.Context
import android.text.format.DateFormat
import java.util.*

class MyDatePickerDialog(
    context: Context,
    listener: OnDateSetListener,
    date: Date,
) : DatePickerDialog(
    context,
    listener,
    Integer.valueOf(DateFormat.format("yyyy", date).toString()),
    Integer.valueOf(DateFormat.format("MM", date).toString())-1,
    Integer.valueOf(DateFormat.format("dd", date).toString())
)