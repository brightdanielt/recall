package com.cauliflower.recall.ui

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.cauliflower.recall.*
import com.cauliflower.recall.databinding.ItemEventBinding
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.extensions.applySize
import com.cauliflower.recall.extensions.defaultDisplayMetrics
import com.cauliflower.recall.extensions.dpToPixel
import com.cauliflower.recall.extensions.toMyFormat
import com.squareup.picasso.Picasso

/**
 * A new style adapter to show event list.
 * */
class EventAdapter : ListAdapter<Event, EventAdapter.EventViewHolder>(DiffCallback()) {

    var onItemLongPressed: ((String?) -> Boolean)? = null

    /**
     * View works as shareElements.
     * IntArray works like shareElements and the elements are ColorInt.
     * */
    var onItemClicked: ((View, Event, IntArray) -> Unit)? = null

    companion object {
        /**
         * The same group the same img height.
         * Group every 2: | 150h, 150h | 250h, 250h | 150h, 150h | 250h, 250h ....
         * */
        const val HEIGHT_GROUP_EVERY = 2

        var isColorFul: Boolean = false

        //Basic color group
        val colors = arrayOf(
            R.color.color1,
            R.color.color2,
            R.color.color3,
            R.color.color4,
            R.color.color5
        )

        /**
         * Repeat the color order every 4.
         * Group every 4: | c1, c2, c3, c4 | c1, c2, c3, c4 | c1, c2, c3, c4  ....
         * */
        val COLOR_GROUP_EVERY = colors.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val binding = ItemEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EventViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = getItem(position)
        holder.bindData(event)
        holder.flipLayout(position % 2)
        holder.stretchLayout(position)
        holder.coloringLayout(position)
        onItemLongPressed?.let { holder.itemView.setOnLongClickListener { it(holder.path) } }
        onItemClicked?.let {
            holder.itemView.setOnClickListener {
                it(holder.binding.imgInRing, event, holder.binding.imgRing.sharedColors)
            }
        }
    }

    class EventViewHolder(
        val binding: ItemEventBinding
    ) : ViewHolder(binding.root) {
        var path: String? = null

        fun bindData(event: Event) {
            binding.tvDate.text = event.time.toMyFormat()
            binding.tvTitle.text = event.title
            if (event.imagePathList.isNullOrEmpty()) {
                binding.imgInRing.setImageResource(R.drawable.ic_baseline_image_24)
            } else {
                path = event.imagePathList!![0]
                Picasso.get().load(path).placeholder(R.drawable.ic_baseline_image_24)
                    .into(binding.imgInRing)
            }
        }

        /**
         * @param direction if [RingImageView.DIRECTION_TO_TOP] , widgets are top of the RingImageView,
         * if [RingImageView.DIRECTION_TO_BOTTOM] , widgets are bottom of the RingImageView.
         * */
        fun flipLayout(direction: Int) {
            val set = ConstraintSet()
            set.clone(binding.root)
            val margin = 8.dpToPixel(itemView.context)
            when (direction) {
                RingImageView.DIRECTION_TO_TOP -> {
                    set.clear(R.id.tv_title, ConstraintSet.TOP)
                    set.clear(R.id.underline_date, ConstraintSet.TOP)
                    set.clear(R.id.tv_date, ConstraintSet.TOP)
                    set.connect(
                        R.id.tv_title, ConstraintSet.BOTTOM, R.id.img_ring, ConstraintSet.TOP,
                        margin
                    )
                    set.connect(
                        R.id.underline_date, ConstraintSet.BOTTOM, R.id.tv_title, ConstraintSet.TOP,
                        margin
                    )
                    set.connect(
                        R.id.tv_date, ConstraintSet.BOTTOM, R.id.underline_date, ConstraintSet.TOP,
                        margin
                    )
                }
                RingImageView.DIRECTION_TO_BOTTOM -> {
                    set.clear(R.id.tv_title, ConstraintSet.BOTTOM)
                    set.clear(R.id.underline_date, ConstraintSet.BOTTOM)
                    set.clear(R.id.tv_date, ConstraintSet.BOTTOM)
                    set.connect(
                        R.id.tv_title, ConstraintSet.TOP, R.id.underline_date, ConstraintSet.BOTTOM,
                        margin
                    )
                    set.connect(
                        R.id.underline_date, ConstraintSet.TOP, R.id.tv_date, ConstraintSet.BOTTOM,
                        margin
                    )
                    set.connect(
                        R.id.tv_date, ConstraintSet.TOP, R.id.img_ring, ConstraintSet.BOTTOM,
                        margin
                    )
                }
            }
            set.applyTo(binding.root)
            binding.imgRing.setDirection(direction)
        }

        /**
         * Adjust [imgRing]'s height according to item's group.
         *
         * */
        fun stretchLayout(position: Int) {
            val heightPx = itemView.defaultDisplayMetrics().heightPixels
            if (position % (HEIGHT_GROUP_EVERY * 2) < HEIGHT_GROUP_EVERY) {
                binding.imgRing.applySize(height = (heightPx * 0.35).toInt())
            } else {
                binding.imgRing.applySize(height = (heightPx * 0.55).toInt())
            }
        }

        fun coloringLayout(position: Int) {
            val g = GradientDrawable()
            val colorStart = colors[position % COLOR_GROUP_EVERY]
            val colorEnd = when {
                isColorFul -> colors[(position + 2) % COLOR_GROUP_EVERY]
                else -> colorStart
            }
            binding.imgRing.setGradientColor(colorStart, colorEnd)
            binding.underlineDate.setBackgroundResource(colorEnd)
        }
    }
}

private class DiffCallback : DiffUtil.ItemCallback<Event>() {
    override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem == newItem
    }
}