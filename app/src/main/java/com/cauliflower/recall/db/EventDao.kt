package com.cauliflower.recall.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

/**
 * To access data in Room.
 * */
@Dao
interface EventDao {

    @Query("SELECT * FROM event ORDER BY time DESC")
    fun getAllEvent(): Flow<List<Event>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addEvent(event: Event)

    @Query("DELETE FROM event WHERE id=:id")
    suspend fun deleteEvent(id: Int)
}