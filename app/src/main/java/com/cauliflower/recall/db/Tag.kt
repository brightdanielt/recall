package com.cauliflower.recall.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "tag")
data class Tag(@PrimaryKey @ColumnInfo val name: String) : Serializable

val NO_TAG = Tag("")