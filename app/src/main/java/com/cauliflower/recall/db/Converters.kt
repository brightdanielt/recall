package com.cauliflower.recall.db

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.ByteArrayOutputStream
import java.util.*


/**
 * Sometimes, you need your app to store a custom data type in a single database column.
 * You support custom types by providing type converters, which are methods that tell Room
 * how to convert custom types to and from known types that Room can persist.
 * You identify type converters by using the @TypeConverter annotation.
 * */
class Converters {

    @TypeConverter
    fun fromBitmap(bitmap: Bitmap): ByteArray {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        return outputStream.toByteArray()
    }

    @TypeConverter
    fun toBitmap(byteArray: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun fromTags(tags: List<Tag>?): String? {
        return tags?.let { Gson().toJson(it) }
    }

    @TypeConverter
    fun toTags(value: String?): List<Tag>? {
        return value?.let {
            val type = object : TypeToken<List<Tag>>() {}.type
            Gson().fromJson(it, type)
        }
    }

    @TypeConverter
    fun fromStrings(list: List<String>?): String? {
        return list?.let { Gson().toJson(it) }
    }

    @TypeConverter
    fun toStrings(value: String?): List<String>? {
        return value?.let {
            val type = object : TypeToken<List<String>>() {}.type
            Gson().fromJson(it, type)
        }
    }

}