package com.cauliflower.recall.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TagDao {

    @Query("SELECT * FROM tag ORDER BY name ASC")
    fun getLiveDataTags(): LiveData<List<Tag>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTag(tag: Tag): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTags(tags: List<Tag>)

    @Query("DELETE FROM tag WHERE name=:name")
    suspend fun deleteTag(name: String)

}