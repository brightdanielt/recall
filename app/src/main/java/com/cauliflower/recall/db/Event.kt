package com.cauliflower.recall.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

/**
 * As literal meaning, if you recall something, the "something" is a event.
 * */
@Entity(tableName = "event")
data class Event(
    @ColumnInfo var time: Date,
    @ColumnInfo var title: String,
    @ColumnInfo var description: String? = null,
    @ColumnInfo var tagList: List<Tag>? = null,
    @ColumnInfo var imagePathList: List<String>? = null
) : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    fun hasTag(tags: List<Tag>): Boolean {
        if (tags.isEmpty() || tags.contains(NO_TAG)) return true
        tagList?.let {
            for (item in it) for (tag in tags) {
                if (item.name == tag.name) return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "id: $id, " +
                "time: $time, " +
                "title: $title, " +
                "description: $description, " +
                "tagList: $tagList, " +
                "images: $imagePathList"
    }
}