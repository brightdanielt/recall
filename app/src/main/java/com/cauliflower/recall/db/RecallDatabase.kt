package com.cauliflower.recall.db

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.cauliflower.recall.di.DatabaseModule.database
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

@Database(entities = [Event::class, Tag::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class RecallDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDao
    abstract fun tagDao(): TagDao

    class RecallDatabaseCallback(private val context: Context, private val scope: CoroutineScope) :
        RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            val uriSample = getUriSample()
            database.let {
                scope.launch {
                    populateFirstTag(it.tagDao())
                    populateFirstEvent(uriSample, it.eventDao())
                }
            }
        }

        private suspend fun populateFirstTag(dao: TagDao) = dao.addTags(
            listOf(Tag("First time"), Tag("Second time"))
        )

        /**
         * Add the first Event as an example.
         * */
        private suspend fun populateFirstEvent(path: String, dao: EventDao) {
            dao.addEvent(
                Event(
                    Date(),
                    "第一個事件",
                    "歡迎加入Recall，試著點擊主畫面右下角的＋符號，以新增您的回憶。",
                    arrayListOf(Tag("First time")),
                    arrayListOf(path)
                )
            )
            for (i in 0 until exTitle.size) {
                dao.addEvent(
                    Event(
                        exDate[i],
                        exTitle[i],
                        exContent[i],
                        imagePathList = exPic[i]?.let { arrayListOf(it) }
                    )
                )
            }
        }

        private fun getUriSample() =
            "https://www.soft4fun.net/wp-content/uploads/2014/03/ANDROID.png"

        companion object {
            val exTitle = arrayListOf(
                "Android 1.0",
                "Android 1.1",
                "Android 1.5 Cupcake",
                "Android 1.6 Donut",
                "Android 2.0-2.1 Eclair",
                "Android 2.2 Froyo",
                "Android 2.3 Gingerbread",
                "Android 3.0 Honeycomb",
                "Android 4.0 Ice Cream Sandwich",
                "Android 4.1-4.3 Jelly Bean",
                "Android 4.4 KitKat",
                "Android 5.0 Lollipop",
                "Android 6.0 Marshmallow",
                "Android 7.0 Nougat",
                "Android 8.0 Oreo",
                "Android 9.0 Pie",
                "Android 10",
                "Android 11",
            )

            val exDate = arrayListOf(
                Date(2008 - 1900, 9 - 1, 23),
                Date(2009 - 1900, 2 - 1, 9),
                Date(2009 - 1900, 4 - 1, 27),
                Date(2009 - 1900, 9 - 1, 15),
                Date(2009 - 1900, 10 - 1, 26),
                Date(2010 - 1900, 5 - 1, 20),
                Date(2010 - 1900, 12 - 1, 6),
                Date(2011 - 1900, 2 - 1, 22),
                Date(2011 - 1900, 10 - 1, 18),
                Date(2012 - 1900, 7 - 1, 9),
                Date(2013 - 1900, 12 - 1, 31),
                Date(2014 - 1900, 11 - 1, 12),
                Date(2015 - 1900, 10 - 1, 5),
                Date(2016 - 1900, 8 - 1, 22),
                Date(2017 - 1900, 8 - 1, 21),
                Date(2018 - 1900, 8 - 1, 6),
                Date(2019 - 1900, 9 - 1, 3),
                Date(2020 - 1900, 9 - 1, 8)
            )

            val exContent = arrayListOf(
                "The first smartphone with an Android operating system was launched in 2008 and its name was HTC Dream, also known as T-Mobile G1. It had a pop-up QWERTY keypad and a touchscreen display. The smartphone did not go well it had many flaws. It had Android 1.0 in it and the beginning of Android’s journey started with it. This version of Android (in fact the first 2 versions) did not have any official names or code names.",

                "Even though the first two public versions of Android (1.0 and 1.1) did not have any code names, Android 1.1 was unofficially called Petit Four. This was released in February 2009, ie just 4 months after the launch of Android 1.0 version, but there were no major changes than in the previous version. However, one important thing that turned in favor of Android with this release was that it was able to prove the easiness to users to install newer updates with incremental features, as no other platform had that sort of a capability then. This was evident later, when Android released 4 versions in the year 2009 itself, including the version 1.1.",

                "It was version 1.5 that came with the name Cupcake and this pattern of naming Android version is opted by Google till now. The cupcake was released in April 2009. Many features and improvements were included in it. Few of its features available on Android even today such as the ability to upload videos to YouTube, support for third-party keyboards, and feature like automatically rotating phone’s screen to the right positions.",

                "Google launched the next version just five months later. It was Android 1.6 Donut. The main feature included in Donut was that it supported carriers that used CDMA based networks. This was a big plus point, as it allowed all carriers across the world to sell smartphones with Android OS.\n" +
                        "\n" +
                        "It also included features like quick switching between the Cameras, Camcorder, and Gallery that could streamline the image-capture experience. It also introduced the Quick Search Box. Also, there were features like Power Controlling widget that could manage Wi-Fi, Bluetooth, Global Positioning System (GPS), etc.\n" +
                        "\n" +
                        "One of the smartphones, Dell Streak had Donut operating system. It had a 5-inch screen that was huge at that time. It was not well received by the public.",

                "In October 2009, Google launched the second version of Android and named it eclair. It was the first Android version with text-to-speech support. It also introduced multiple account support, live wallpapers, navigation with Google Maps, and many other new features.\n" +
                        "\n" +
                        "The first smartphone with the Android 2.0 version was the Motorola Droid, which was also the first Android phone that was sold by Verizon wireless.",

                "The next version, Froyo, short form for Frozen Yogurt was launched in May 2010. It was in this version that Wi-Fi mobile hotspot functions was introduced. It also included many other features such as flash support, push notifications via Android Cloud to Device Messaging (C2DM) service, and more.\n" +
                        "\n" +
                        "Google’s Nexus one earlier had Android 2.1 but quickly it was updated to Android 2.2 Froyo.",

                "The Android 2.3 Gingerbread was launched in September 2010. A number of features were included in this version updated UI design that provided increased efficiency and ease-of-use. It had support for extra-large screen sizes and resolution. More features such as native support for SIP VoIP internet telephones, improved text inputs using the virtual keyboard, better text suggestions and voice input capability were added. One of the key features was its support for using NFC (near field communication) functions for smartphones.\n" +
                        "\n" +
                        "The first Android smartphone with this version was the Nexus S. It was co-developed by Google and Samsung. This version also laid the foundation for a selfie. In this, multiple cameras were supported and also had video chat support within Google Talk.",

                "The next version was something special. Android version 3.0 Honeycomb was launched to be installed only for tablets and mobile devices with larger screens. It was launched in February 2011. Androids rival, Apple launched iPad in 2010. Honeycomb was a direct response to Apple. Google aimed for features that could not be handled by smartphones with smaller screens. But Honeycomb ended up as a version that not really required. Most of the features of Honeycomb were integrated with the next major version of Android.",

                "Ice Cream Sandwich was launched in October 2011. It had many features. Features of the previous version, Honeycomb, were integrated with the Ice Cream Sandwich version. This version was the first to introduce the support the feature to unlock the phone using its camera. This feature will evolve a lot in the upcoming years. Other notable changes with Ice Cream Sandwich included support for all the on-screen buttons, the ability to monitor the mobile and Wi-Fi data usage, and swipe gestures to dismiss notifications and browser tabs.",

                "Google launched Android 4.1 with the Jelly Bean label on June 2012. Two more versions under the Jelly Bean label, Android 4.2 & 4.3 were released by Google in October 2012 & July 2013, respectively. The notification part was improved a lot in this version. Full support for Google Chrome (Android version) was included in Android 4.2.  Android’s touch responsiveness was also improved. Jelly Bean was collectively the first Android version to support emoji and screensavers that are natively done.\n" +
                        "\n" +
                        "Nexus 7 tablets had Jelly Bean installed in it. Many Android smartphones still use this version of Android.",

                "Google contacted Nestle, the maker of KitKat chocolate, asking if they could use the chocolate bar’s name for the next version of Android. Nestle agreed to this and Android 4.4 KitKat was launched in September 2013. KitKat did not have many features. But one main feature was that KitKat could run on smartphones with even a 512 MB RAM. It was because KitKat used the Android Runtime (ART), though experimental, instead of the DVM (Dalvik Virtual Machine) originally used by Android. This expanded the market share of Android to the next level. The phone makers could now run Android on cheaper smartphones.\n" +
                        "\n" +
                        "Google’s Nexus 5 had the KitKat version of Android. KitKat still runs on many smartphones around the world.",

                "Android 5.0 Lollipop was launched in June 2014. Google’s new Material Design language was first introduced in Lollipop, which brought some major aesthetic changes to Android UI. It included changes in UIs like a revamped navigation bar and better-style notifications for the lock-screen etc. It brought the Flat Design concept into play. Google created more enhancements to Android devices’ battery life with a Doze mode where background apps are killed once the show is turned off.\n" +
                        "\n" +
                        "Google’s Nexus 6 and Nexus 9 tablets were the first to use Lollipop.",

                "First, the Android 6.0 version was to be called Macadamia Nut Cookie, but it was released as Marshmallow in May 2015. It included many new features like an app drawer which was vertically scrolling, along with Google Now available on Tap. This was the first version that had native support for unlocking of the smartphone with biometric; fingerprint authentication. USB Type C support was included and Android pay was also introduced in Marshmallow.\n" +
                        "\n" +
                        "Google’s Nexus 6P and Nexus 5X smartphones were the first smartphones that had Marshmallow.",

                "Android 7.0 Nougat was released in August 2016. It came out with multitasking features, especially for smartphones with bigger screens. It included split-screen and fast switching between apps.\n" +
                        "\n" +
                        "Many changes behind the scenes were also made by Google such as switching to a new JIT compiler that could speed up apps. \n" +
                        "\n" +
                        "Google’s own smartphone, the Pixel, and Pixel XL, and LG V20 came out with Android 7.0 Nougat.",

                "This was the second time Google used a trademark name for it’s Android version, first being KitKat. Android 8.0 Oreo was released on August 2017. It included many visual changes such as native support for picture-in-picture mode, new autofill APIs that could help in better managing the passwords and fill data, notification channels, and much more.",

                "The next major version was released in August 2018. It came with a lot of new features and improvements. The new home-button was added in this version. When swiped up, it brings the apps used recently, a search bar and suggestions of five apps at the bottom. There was a new option added of swiping left to see the currently running apps. Improvements in battery life were also made in this version. Shush, a new feature was also added. It automatically puts the smartphone in Do not disturb mode. Many more features were also added.",

                "Finally, Google opted to drop the tradition of naming the Android version after sweets and desserts. It was launched in September 2019. A number of features were added such as support for the upcoming foldable smartphones with flexible displays. Android 10 also has a system-wide dark mode, along with the newly introduced navigation control using gestures, the feature for smart reply for all the messaging apps, and a sharing menu that is more effective. The control over app-based permissions is also more in it.",

                "Google made the initial release of Android 11 on the 8th of September 2020. Even during the peak times of the COVID-19 pandemic, the Open Handset Alliance led by Google did an impressive task. Like the previous version, Android 11 is not a visual overhaul. The design stays true to the philosophy that Google had started a couple of iterations back. On the other hand, Android 11 attempted to optimize the user experience by keeping Google services close to the smartphone experience. Thanks to Google’s efforts to speed up the rolling-out procedure, Android 11 runs on a considerable number of devices from different manufacturers. Most devices launched after September 2020 are shipping with Android 11 or offer an easy-upgrade option. "
            )

            val exPic: ArrayList<String?> = arrayListOf(
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2008.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2009.png",
                "https://cdn.acodez.in/wp-content/uploads/2019/09/Android-1.5-Cupcake-768x442.png",
                "https://cdn.acodez.in/wp-content/uploads/2019/09/Android-1.6-Donut-768x442.png",
                "https://cdn.acodez.in/wp-content/uploads/2019/09/Android-2.0-2.1-Eclair-768x442.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2010.png",
                "https://cdn.acodez.in/wp-content/uploads/2019/09/Android-2.3-Gingerbread-768x442.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2011.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2011-2013.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2012-768x483.png",
                "https://cdn.acodez.in/wp-content/uploads/2019/09/Android-4.4-KitKat-768x442.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2014.png",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Android_Marshmallow_logo.png/240px-Android_Marshmallow_logo.png",
                "https://1.bp.blogspot.com/-TfaGPilbLMk/V7uJhtQU6PI/AAAAAAAAF3Q/JHlJpO5eyhQeRoIKEaZqnu_26lNzRGJvQCLcB/s400/nougat_16_9.png",
                "https://developer.android.com/about/versions/oreo/images/o-hero.png",
                "https://developer.android.com/images/home/android-p-clear-bg-with-shadow.png",
                "https://1000logos.net/wp-content/uploads/2016/10/Android-Version-Logo-2019-768x483.png",
                "https://static.wikia.nocookie.net/logopedia/images/b/b5/Android11Logo.svg/revision/latest/scale-to-width-down/150?cb=20200605162537"
            )
        }
    }

}