package com.cauliflower.recall

import androidx.lifecycle.LiveData
import com.cauliflower.recall.db.Event
import com.cauliflower.recall.db.EventDao
import com.cauliflower.recall.db.Tag
import com.cauliflower.recall.db.TagDao
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RecallRepository @Inject constructor(
    private val eventDao: EventDao, private val tagDao: TagDao
) {

    val allEvent: Flow<List<Event>> = eventDao.getAllEvent()
    val allLiveDataTags: LiveData<List<Tag>> = tagDao.getLiveDataTags()

    suspend fun addEvent(event: Event) = eventDao.addEvent(event)
    suspend fun deleteEvent(id: Int) = eventDao.deleteEvent(id)
}