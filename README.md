What's this?
========
This is my side project and the main function is to record something which is important to users.

Why I create this project?
========
Few weeks ago, my buddies and I talk about date of the past gatherings which are more than 10 times and we could not even remember chronologically of the gatherings, however we are people who care about sympathy for brotherhood so it's important to remember these and I decide to create this project to record something important, something like treasure.

Project progress
========
It's still in development however the infrastructures are done, the final function is to access database in cloud.

Thoughts after project
========
In fact, it spend me a lot of time in designing UX, I am not good at it and I also do little preparation on it.<br>
It teach me a lesson: If I am not sure of it and I should well prepare for it. <br>
Also, thanks my ex-coworker who gave me advice about animation, thanks material design, it's a great web site.

Snapshot of this application
========
This demo takes Android version history as the subject.<br>
[![IMAGE ALT TEXT](http://img.youtube.com/vi/QZMZsX6PBj8/0.jpg)](http://www.youtube.com/watch?v=QZMZsX6PBj8 "Recall demo")<br>

A demo in dark theme.<br>
[![IMAGE ALT TEXT](http://img.youtube.com/vi/bZCi-DQhjeQ/0.jpg)](http://www.youtube.com/watch?v=bZCi-DQhjeQ "Recall demo in dark theme")


Developer's notes
=======
2022/02/14<br>
Today I solved a issue about how to show app's content behind the system bars,
the main knowledge is not hiding status bar nor making app fullscreen but "edge-to-edge".
In fact, the two methods mentioned above is possible to fulfill the feature in some devices but it won't
work in pixel 6 and I also tried the both way, you can check it in git reversion number e70832db8d8c0c22ddbfc65f6dfa106e102001ae,
If you are interesting in it, check this [Display content edge-to-edge in your app].


[Display content edge-to-edge in your app]:https://developer.android.com/training/gestures/edge-to-edge
